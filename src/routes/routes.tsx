import { createBrowserRouter, RouterProvider } from "react-router-dom";
import App from "../App";
import DetailSection from "../domains/detail-section";
import CreateUser from "../domains/create-user";

const router = createBrowserRouter([
  { path: "/", element: <App /> },
  { path: "/detail/:userId", element: <DetailSection /> },
  { path: "/crear", element: <CreateUser /> },
]);

const Routes = () => {
  return <RouterProvider router={router} />;
};

export default Routes;
