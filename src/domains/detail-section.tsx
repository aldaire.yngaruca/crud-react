import { useQuery, useQueryClient } from "react-query";
import { Link, useParams } from "react-router-dom";
import { getUserById } from "../api/people";
import { useState } from "react";
import { Button, Card, Spinner } from "react-bootstrap";

const DetailSection = () => {
  let { userId } = useParams();

  const { data, isLoading } = useQuery({
    queryKey: ["people", userId],
    queryFn: () => getUserById(userId),
  });

  return (
    <div className="container">
      <h1>Detalle del Usuario</h1>
      {isLoading ? (
        <Spinner />
      ) : (
        <div className="row text-align-center">
          <Card style={{ width: "18rem" }}>
            <Card.Body>
              <Card.Title>
                Nombre: {data.name} {data.lastName}
                <br />
                Edad: {data.age}
              </Card.Title>
              <Card.Text>{data.job}</Card.Text>
              <Link to="/">Regresar</Link>
            </Card.Body>
          </Card>
        </div>
      )}
    </div>
  );
};

export default DetailSection;
