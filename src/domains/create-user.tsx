import PeopleForm from "../components/PeopleForm";

const CreateUser = () => {
  return (
    <div className="container">
      <h1 className="text-center mb-3 text-light">Simple CRUD</h1>
      <div className="row">
        <div className="col-sm-12">
          <PeopleForm />
        </div>
      </div>
    </div>
  );
};

export default CreateUser;
