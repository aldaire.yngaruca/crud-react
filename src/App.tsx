import People from "./components/PeopleList";
import "./app.css";

const App = () => {
  return (
    <div className="container">
      <h1 className="text-center m-5 text-light">Simple CRUD</h1>

      <div className="row">
        <div className="col-sm-12">
          <People />
        </div>
      </div>
    </div>
  );
};

export default App;
