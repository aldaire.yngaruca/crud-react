import Modal from "react-bootstrap/Modal";
import PeopleForm from "./PeopleForm";

const EditPeopleModal = (props: any) => {
  const { showModal, setShowModal, infoUser } = props;

  const handleClose = () => {
    setShowModal(!showModal);
  };

  return (
    <Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>
          {infoUser.name} {infoUser.lastName}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <PeopleForm
          isEditing={true}
          data={infoUser}
          handleClose={handleClose}
        />
      </Modal.Body>
    </Modal>
  );
};

export default EditPeopleModal;
