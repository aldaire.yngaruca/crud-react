import { describe, expect, it } from "vitest";
import { render, screen, fireEvent } from "@testing-library/react";
import ButtonActions from "./ButtonActions";

const Prueba = () => {
  return (
    <div>
      <p>Editar</p>
      <p>Eliminar</p>
    </div>
  );
};

describe("ButtonActions", () => {
  it("should render correctly", () => {
    const userId = "1";
    render(<ButtonActions userId={userId} />);

    const buttonEdit = screen.getByText("Editar");
    expect(buttonEdit).toBeDefined();
    // const buttonDetail = screen.getByText("detalle");
    // const buttonDelete = screen.getByText("eliminar");
  });
});
