import React from "react";
import { useState, useEffect } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createUser, updateUser } from "../api/people";
import { createUserInterface } from "../interfaces/user";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export type PeopleFormProps = {
  isEditing?: boolean;
  data?: any;
  handleClose?: any;
};

const PeopleForm: React.FC<PeopleFormProps> = (props: PeopleFormProps) => {
  const { data, isEditing, handleClose } = props;
  const [initValues, setInitValues] = useState<createUserInterface>({
    name: "",
    lastName: "",
    age: 0,
    job: "",
  });

  const queryClient = useQueryClient();

  const addPeopleMutation = useMutation({
    mutationFn: createUser,
    onSuccess: () => {
      queryClient.invalidateQueries("people");
    },
  });

  const updatePeopleMutation = useMutation({
    mutationFn: updateUser,
    onSuccess: () => {
      queryClient.invalidateQueries("people");
    },
  });

  const onChangeInput = (e: any) => {
    setInitValues({
      ...initValues,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (isEditing) {
      updatePeopleMutation.mutate({ ...initValues });
      handleClose();
    } else {
      addPeopleMutation.mutate({
        ...initValues,
      });

      setInitValues({
        name: "",
        lastName: "",
        age: 0,
        job: "",
      });
    }
  };

  useEffect(() => {
    if (isEditing) {
      setInitValues(data);
    }
  }, [data]);

  return (
    <div className="card p-4">
      <h2 className="mb-4">{isEditing ? "Edit User" : "Create User"}</h2>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingresa tu nombre"
            name="name"
            onChange={onChangeInput}
            value={initValues.name}
            required
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Apellido</Form.Label>
          <Form.Control
            type="text"
            name="lastName"
            value={initValues.lastName}
            onChange={onChangeInput}
            placeholder="Ingresa tu apellido"
            required
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Edad</Form.Label>
          <Form.Control
            type="number"
            name="age"
            value={initValues.age}
            onChange={onChangeInput}
            placeholder="28"
            required
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Puesto</Form.Label>
          <Form.Control
            type="text"
            name="job"
            value={initValues.job}
            onChange={onChangeInput}
            placeholder="Ingresa el puesto"
            required
          />
        </Form.Group>
        <div className="d-grid gap-2">
          <Button variant="primary" type="submit">
            {isEditing ? "Edit User" : "Add User"}
          </Button>
          {!isEditing && <Link to="/">Regresar</Link>}
        </div>
      </Form>
    </div>
  );
};

PeopleForm.defaultProps = {
  isEditing: false,
  data: {},
  handleClose: () => {},
};
export default PeopleForm;
