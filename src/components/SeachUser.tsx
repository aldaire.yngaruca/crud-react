import { useSearchUserHook } from "../hooks/use-seach-user-hook";
import { Button, Form, InputGroup } from "react-bootstrap";

const SearchUser = (props: any) => {
  const { setDataFromApi } = props;
  const { userSearch, onChangeInput, searchUser, clearSearch } =
    useSearchUserHook(setDataFromApi);

  return (
    <div>
      <InputGroup className="mb-3">
        <Form.Control
          value={userSearch}
          onChange={onChangeInput}
          name="userSearch"
          placeholder="Ingresa Nombre"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <Button variant="secondary" onClick={searchUser}>
          Buscar
        </Button>
        <Button variant="outline-secondary" onClick={clearSearch}>
          Limpiar
        </Button>
      </InputGroup>
    </div>
  );
};

export default SearchUser;
