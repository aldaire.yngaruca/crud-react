import React from "react";
import Badge from "react-bootstrap/Badge";
import useButtonActions from "../hooks/use-button-actiones-hook";
import EditPeopleModal from "./EditPeopleModal";

interface Props {
  userId: string;
}

const ButtonActions: React.FC<Props> = ({ userId }) => {
  const {
    infoUser,
    showModal,
    showInfo,
    deleteUserId,
    setShowModal,
    showUser,
  } = useButtonActions();

  return (
    <>
      <td style={{ cursor: "pointer" }}>
        <Badge bg="primary" onClick={() => showInfo(userId)}>
          editar
        </Badge>{" "}
        <Badge bg="secondary" onClick={() => showUser(userId)}>
          detalle
        </Badge>{" "}
        <Badge bg="danger" onClick={() => deleteUserId(userId)}>
          eliminar
        </Badge>
      </td>

      <EditPeopleModal
        showModal={showModal}
        setShowModal={setShowModal}
        infoUser={infoUser}
      />
    </>
  );
};

export default ButtonActions;
