import { getUserInterface } from "../interfaces/user";
import Table from "react-bootstrap/Table";
import { Spinner } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { usePeopleHook } from "../hooks/use-people-hook";
import SearchUser from "./SeachUser";
import ButtonActions from "./ButtonActions";

const People = () => {
  const { userList, setDataFromApi, isLoading } = usePeopleHook();

  return (
    <div className="card p-4">
      <h2 className="mb-4">User List</h2>

      {isLoading ? (
        <Spinner animation="border" variant="dark" />
      ) : (
        <div>
          <div className="d-flex  justify-content-between mb-5">
            <SearchUser setDataFromApi={setDataFromApi} />
            <Button variant="primary" size="sm">
              <Link to="/crear" className="text-white">
                Crear usuario
              </Link>
            </Button>
          </div>

          <Table striped bordered hover size="sm" responsive>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Puesto</th>
                <th>Acctiones</th>
              </tr>
            </thead>

            <tbody>
              {userList?.length > 0 ? (
                userList?.map((user: getUserInterface) => {
                  return (
                    <tr key={user._id}>
                      <td>{user.name}</td>
                      <td>{user.lastName}</td>

                      <td>{user.job}</td>

                      <ButtonActions userId={user._id} />
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan={4}>No hay datos</td>
                </tr>
              )}
            </tbody>
          </Table>
        </div>
      )}
    </div>
  );
};

export default People;
