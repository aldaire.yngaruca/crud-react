import React, { FC, useMemo, useReducer } from "react";
import { userReducer } from "./reducers/user";

export interface IAppStore {
  user: any;
}

interface IAppContext {
  store: IAppStore;
  actions: {
    dispatchUser: React.Dispatch<any>;
  };
}

export const AppContext = React.createContext<IAppContext>({
  store: {} as any,
  actions: {} as any,
});

interface AppContextWrapperProps {
  children: any;
}

const AppContextWrapper: FC<AppContextWrapperProps> = ({ children }) => {
  const [user, dispatchUser] = useReducer(userReducer, {});
  const config = {
    store: {
      user,
    },
    actions: {
      dispatchUser,
    },
  };
  const providerValue = useMemo(() => ({ config }), [config]);

  return (
    <AppContext.Provider value={providerValue.config}>
      {children}
    </AppContext.Provider>
  );
};

export default AppContextWrapper;
