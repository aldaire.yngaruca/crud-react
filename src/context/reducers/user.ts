import { USERS_ACTION, RESET_ACTION } from '../staticData'

export function userReducer(state: any, action: any) {
    switch (action.type) {
        case USERS_ACTION.set:
            return { ...state, ...action.payload };

        case RESET_ACTION:
            return {};

        default:
            throw new Error();
    }
}
