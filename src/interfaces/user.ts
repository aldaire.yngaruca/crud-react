export interface createUserInterface {
    name: string,
    lastName: string,
    age: number,
    job: string
}

export interface getUserInterface extends createUserInterface {
    _id: string,
}