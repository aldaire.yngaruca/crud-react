import axios from "axios";
import { createUserInterface } from "../interfaces/user";

const users = axios.create({
  baseURL: "http://localhost:8000/api/users",
});

export const getUsers = async () => {
  const res = await users.get("/");
  const { data } = res.data;
  return data;
};

export const createUser = async (user: createUserInterface) => {
  const res = await users.post("/create", user);
  return res;
};

export const deleteUser = async (userId: string) => {
  const res = await users.delete(`/delete/${userId}`);
  return res;
};

export const updateUser = async (data: any) => {
  const { _id: userId } = data;
  const res = await users.put(`/update/${userId}`, data);
  return res;
};

export const getUserById = async (userId: any) => {
  const res = await users.get(`/get/${userId}`);
  const { data } = res.data;
  return data;
};

export const getUserByName = async (userName: any) => {
  const res = await users.get(`/name/${userName}`);
  const { data } = res.data;
  return data;
};
