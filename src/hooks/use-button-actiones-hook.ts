import { useState, useContext } from "react";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { deleteUser } from "../api/people";
import { AppContext } from "../context/global-provider";
import { useNavigate } from "react-router-dom";

const useButtonActions = () => {
  const [showModal, setShowModal] = useState(false);
  const [infoUser, setInfoUser] = useState({});
  const navigate = useNavigate();

  const { store } = useContext(AppContext);
  const {
    user: { data },
  } = store;

  const queryClient = useQueryClient();

  const deletePeopleMutation = useMutation({
    mutationFn: deleteUser,
    onSuccess: () => {
      queryClient.invalidateQueries("people");
    },
  });

  const deleteUserId = async (id: string) => {
    try {
      await deletePeopleMutation.mutate(id);
    } catch (e) {
      throw new Error("Error to delete user");
    }
  };

  const showInfo = (id: string) => {
    const userInfo = data.find((x: any) => {
      return x._id === id;
    });
    setInfoUser(userInfo);
    setShowModal(true);
  };

  const showUser = (id: string) => {
    navigate(`/detail/${id}`);
  };

  return {
    infoUser,
    showModal,
    showInfo,
    deleteUserId,
    setShowModal,
    showUser,
  };
};

export default useButtonActions;
