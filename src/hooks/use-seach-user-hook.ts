import { useContext, useState, ChangeEvent } from "react";
import { useQuery } from "react-query";
import { getUserByName } from "../api/people";
import { AppContext } from "../context/global-provider";

export const useSearchUserHook = (setDataFromApi: any) => {
  const [userSearch, setUserSearch] = useState("");

  const getUsers = useQuery({
    queryKey: ["people", userSearch],
    queryFn: () => getUserByName(userSearch),
    enabled: false,
  });

  const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    setUserSearch(e.target.value);
  };

  const searchUser = async () => {
    if (userSearch.length > 2) {
      const { data } = await getUsers.refetch();
      setDataFromApi(data);
    }
  };

  const { store } = useContext(AppContext);

  const clearSearch = () => {
    setUserSearch("");
    setDataFromApi(store.user.data);
  };

  return {
    userSearch,
    onChangeInput,
    searchUser,
    clearSearch,
  };
};
