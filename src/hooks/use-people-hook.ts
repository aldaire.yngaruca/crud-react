import { useQuery } from "react-query";
import { getUsers } from "../api/people";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "../context/global-provider";
import { USERS_ACTION } from "../context/staticData";
import { getUserInterface } from "../interfaces/user";
export const usePeopleHook = () => {
  const [userList, setUserList] = useState([]);

  const { data, isLoading } = useQuery({
    queryKey: ["people"],
    queryFn: getUsers,
  });

  const { actions } = useContext(AppContext);

  useEffect(() => {
    actions.dispatchUser({
      type: USERS_ACTION.set,
      payload: { data },
    });
    setUserList(data);
  }, [data]);

  const setDataFromApi = (data: any) => {
    setUserList(data);
  };

  return {
    userList,
    isLoading,
    setDataFromApi,
  };
};
