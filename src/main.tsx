import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import AppContextWrapper from "./context/global-provider";
import "bootstrap/dist/css/bootstrap.min.css";
import Routes from "./routes/routes";
const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <AppContextWrapper>
      <QueryClientProvider client={queryClient}>
        {/* <App /> */}
        <Routes />
        <ReactQueryDevtools />
      </QueryClientProvider>
    </AppContextWrapper>
  </React.StrictMode>
);
