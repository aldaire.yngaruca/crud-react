import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    globals: true,
    environment: "happy-dom",
    coverage: {
      provider: "istanbul", // or 'c8',
      all: true,
    },
  },
});
