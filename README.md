# CRUD WITH REACT AND TS

Este proyecto ha sido creado con el comando npm create vite@latest

## Estructura de Carpetas

A continuación se detalla la estructura de carpetas de React:

    .
    ├── ...
    ├── src
    │    ├── /api
    │    ├── /assets
    │    ├── /components
    │    ├── /context
    │    ├── /domains
    │    ├── /hooks
    │    ├── /interfaces
    │    └── /routes
    └── .env.local

### `npm install`

No olvidar correr el comando <b>npm install</b> antes de correr el proyecto.
El proyecto inicializa con el comando <b>npm run dev</b>.
